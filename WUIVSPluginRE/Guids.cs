﻿// Guids.cs
// MUST match guids.h
using System;

namespace FSL.WUIVSPluginRE
{
    static class GuidList
    {
        public const string guidWUIVSPluginREPkgString = "7a87da74-a2e8-43f9-9086-d44b58595112";
        public const string guidWUIVSPluginRECmdSetString = "5ff3fb55-d110-4244-8f05-2d929ed2a4c9";
        public const string guidToolWindowPersistanceString = "393aa901-1f46-4c9d-8ae5-7eeac7cbc9f0";

        public static readonly Guid guidWUIVSPluginRECmdSet = new Guid(guidWUIVSPluginRECmdSetString);
    };
}