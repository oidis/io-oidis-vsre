﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace FSL.WUIVSPluginRE
{
    public partial class HostWindows : HwndHost
    {
        private static int GWL_STYLE = -16;
        private static UInt32 WS_CHILD = 0x40000000;
        private static UInt32 WS_POPUP = 0x80000000;

        private IntPtr handleHosted = IntPtr.Zero;

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        private static extern UInt32 SetWindowLong(IntPtr hWnd, int nIndex, UInt32 dwNewLong);

        [DllImport("user32.dll")]
        private static extern UInt32 GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern int DestroyWindow(IntPtr hWWnd);

        public HostWindows(string nativeWindowClassName)
        {
            this.handleHosted = FindWindow(nativeWindowClassName, null);

            if (this.handleHosted == IntPtr.Zero)
            {
                throw new Exception("Failed to find the given HWND of window class: " + nativeWindowClassName);
            }
        }

        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            HandleRef href = new HandleRef();

            if (this.handleHosted != IntPtr.Zero)
            {
                UInt32 currentStyles = GetWindowLong(this.handleHosted, HostWindows.GWL_STYLE);
                SetWindowLong(this.handleHosted, HostWindows.GWL_STYLE, currentStyles | HostWindows.WS_CHILD);
                SetParent(this.handleHosted, hwndParent.Handle);
                href = new HandleRef(this, this.handleHosted);
            }

            return href;
        }

        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            if (this.handleHosted != IntPtr.Zero)
            {
                if (DestroyWindow(this.handleHosted) != 0)
                {
                    throw new Exception("Failed destroy win32 window with this handle: " + this.handleHosted);
                }
            }
        }
    }
}
