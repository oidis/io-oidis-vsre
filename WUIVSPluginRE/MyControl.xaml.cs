﻿using System.Windows;
using System.Windows.Controls;

namespace FSL.WUIVSPluginRE
{
    public partial class MyControl : UserControl
    {
        private HostWindows host = null;

        public MyControl()
        {
            InitializeComponent();

            InitializeHost();
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
        }

        private void OnUnload(object sender, RoutedEventArgs e)
        {
        }

        private void InitializeHost()
        {
            if (host == null)
            {
                host = new HostWindows("chromiumRE");

                WindowPanel.Child = host;
            }
        }
    }
}
